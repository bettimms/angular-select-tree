/**
 * Created by PC3 on 3/22/2016.
 */

(function ()
{
    var app = angular.module("app", []);

    function HomeController()
    {
        var vm = this;

        vm.datasource = [{id:1,
            name: "Person 1", children: [{id: "Child 1 "}, {id: "Child 2 "}, {id: "Child 3 "}],
            students: [{name: "Student 1 "}, {name: "Student 2"}, {name: "Student 3"}]
        },
            {
                name: "Person 2",id:1, children: {id: "Child 2"},
                students: [
                    {name: "Student in 1", students: [{name: "Student in 2",students: [{name: "Student 1 ",students: [{name: "Student 1 "}]}]}]}]
            },
            {name: "Person 3",id:1, children: {id: "Child 3"}, students: [{name: "Student 1 "}, {name: "Student 2"}]}];


        vm.allItems = [];

        vm.login = function (form)
        {
            console.log(vm.allItems);
        }
    }

    app.requires.push("select.tree");
    app.controller("HomeController", [HomeController]);

})();
