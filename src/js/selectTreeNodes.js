/**
 * Created by PC3 on 6/12/2016.
 */

(function ()
{
    function selectTreeNodes($parse)
    {
        return {
            restrict: "EA",
            require:"^selectTree",
            controller: "TreeController",
            controllerAs: "treeCtrl",
            link: function (scope, element, attrs, ctrl)
            {
            }
        }
    }

    treeApp.directive("selectTreeNodes", ["$parse",selectTreeNodes]);
}());