/**
 * Created by PC3 on 6/12/2016.
 */

(function ()
{
    function TreeService($compile, $timeout, $templateRequest, $sce)
    {
        var TreeService = {};

        TreeService.compileTemplate = function (template, scope, callback)
        {
            var compiledContent = $compile(template)(scope);
            var timer = $timeout(function ()
            {
                callback(compiledContent);
                $timeout.cancel(timer);
            });
        }
        TreeService.requestTemplate = function (url)
        {
            var templateUrl = $sce.getTrustedResourceUrl(url);
            return $templateRequest(templateUrl);
        }
        TreeService.compileTemplateFromUrl = function (url, scope, callback)
        {
            TreeService.requestTemplate(url).then(function (template)
            {
                TreeService.compileTemplate(template, scope, function (compiledContent)
                {
                    callback(compiledContent);
                });
            }, function ()
            {
                console.log("Template not found!");
            });
        }
        return TreeService;
    }
    treeApp.service("TreeService", ["$compile", "$timeout", "$templateRequest", "$sce", TreeService]);
}());
