/**
 * Created by PC3 on 6/12/2016.
 */

(function ()
{
    function TreeController()
    {
        var vm = this;
        vm.items = [];
        vm.multiple = false;
        vm.actionsBox = true;
        vm.selectedItem = {};
        vm.selectedItems = [];
        vm.childrenKey = undefined;

        vm.onNodeClick = function (node, event)
        {
            selectItem(node);
            setDropdownText(event)
        }

        function selectItem(item)
        {
            item._$selected = !(item._$selected);
            if (vm.multiple)
            {
                vm.selectedItem._$selected = undefined;
                // pushPullItemToCollection(vm.selectedItems, item);
            }
            else
            {
                //Deselect previous item
                vm.selectedItem._$selected = vm.selectedItem === item ? true: false;
                vm.selectedItem = item;
            }
            // toggleParentSelection(item);
        }

        function setDropdownText(event)
        {
            var selectedText = angular.element(event.target).text();

            if (vm.multiple)
            {
                // var displayText = vm.selectedItems.length === 0 ? nothingSelectedText : (vm.selectedItems.length === 1) ? selectedText : vm.selectedItems.length + " items selected";
                // angular.element(event.target).parents(".dropdown").find(".dropdown-toggle").html(displayText + ' <span class="caret pull-right"></span>');
            }
            else
            {
                // angular.element('.dropdown.open .dropdown-toggle').dropdown('toggle');
                angular.element(event.target).parents(".dropdown").find(".dropdown-toggle").html(selectedText + ' <span class="caret pull-right"></span>');
            }
        }

        
        
        vm.selectAll = function(isSelected)
        {
            var selectedItems = [];
            for (var i = vm.items.length - 1; i >= 0; i--)
            {
                var parentItem = vm.items[i];
                parentItem._$selected = isSelected;
                selectedItems.push(parentItem);

                var children = vm.items[i][vm.childrenKey];
                if (children === undefined) continue;
                for (var k = children.length - 1; k >= 0; k--)
                {
                    var childItem = children[k]
                    childItem._$selected = isSelected;
                    selectedItems.push(childItem);
                }
            }
            vm.selectedItems = isSelected ? selectedItems : [];
        }
    }

    treeApp.controller("TreeController", [TreeController]);
}());