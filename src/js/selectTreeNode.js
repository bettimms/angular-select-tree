/**
 * Created by PC3 on 6/12/2016.
 */

(function ()
{
    function selectTreeNode($compile)
    {
        return {
            restrict: "EA",
            require: ["^?ngModel","^selectTreeNodes", "^selectTree"],
            controller: "TreeNodeController",
            controllerAs: "nodeCtrl",
            bindToController:true,
            link: function (scope, element, attrs, ctrls)
            {
                var treeCtrl = ctrls[2];
                var ngModel = ctrls[0];

                var aTag = angular.element(element).find("a")
                aTag.attr("data-toggle","pill");
                aTag.attr("ng-class","{'active':node._$selected}");
                
                angular.element(element).on("click",function (event)
                {
                    event.stopPropagation();
                    var node = ngModel.$viewValue;
                    treeCtrl.onNodeClick(node,event);
                    ngModel.$setViewValue(node);
                });

                $compile(aTag)(scope);
            }
        }
    }

    treeApp.directive("selectTreeNode", ["$compile",selectTreeNode]);
}());