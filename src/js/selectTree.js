/**
 * Created by PC3 on 6/12/2016.
 */

var treeApp = angular.module("select.tree", []);

(function ()
{
    var templateUrl = "js/tree-template.html";

    function selectTree($compile, $parse)
    {
        return {
            restrict: "EA",
            scope: true,
            controller: "TreeController",
            controllerAs: "treeCtrl",
            bindToController: {multiple: "@"},
            transclude: true,
            templateUrl: templateUrl,
            replace: true,
            link: function (scope, element, attrs, ctrl, transcludeFn)
            {
                transcludeFn(scope, function (clone)
                {
                    angular.element(element).find(".dropdown-menu").append(clone);

                    scope.$applyAsync(function changeViewModel()
                    {
                        var ngRepeat = angular.element(element).find("[ng-repeat]").first().attr("ng-repeat");
                        parseRepeatAttr(ngRepeat);

                        //any other li except 1st is for children
                        var childrenRepeatElem = angular.element(element).find("[ng-repeat]")[1];
                        var childrenRepeat  = angular.element(childrenRepeatElem).attr("ng-repeat");
                        var childrenPropArray = childrenRepeat.split(".");
                        scope.treeCtrl.childrenKey = childrenPropArray[childrenPropArray.length-1];



                        if (attrs.multiple === undefined) return;
                        scope.treeCtrl.multiple = (attrs.multiple.length == 0) ? true : JSON.parse(attrs.multiple);

                        if (attrs.actionsBox === undefined) return;
                        scope.treeCtrl.actionsBox = (attrs.actionsBox.length == 0) ? true : JSON.parse(attrs.actionsBox);
                    });
                });


                function parseRepeatAttr(repeatAttr)
                {
                    var ngRepeat = repeatAttr,
                        match = ngRepeat.match(/^\s*(.+)\s+in\s+(.*?)\s*(\s+track\s+by\s+(.+)\s*)?$/),
                        indexString = match[1],
                        collectionString = match[2];

                    scope.$watchCollection(collectionString, function (collection)
                    {
                        scope.treeCtrl.items = collection;
                    });
                }

            }
        }
    }

    treeApp.directive("selectTree", ["$compile", "$parse", selectTree]);
}());